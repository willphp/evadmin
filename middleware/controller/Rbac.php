<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2025 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);

namespace middleware\controller;

use Closure;

/**
 * RBAC验证
 */
class Rbac
{
    public function run(Closure $next): void
    {
        if (!session('?user')) {
            if (IS_AJAX) {
                halt('', 401); // 验证登录
            }
            header('Location:' . url('login/login'));
            exit();
        } elseif (check_auth() === 0) {
            halt('', 403); // 验证权限
        }
        $next();
    }
}