<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
class Dict extends Widget
{
    protected string $tag = 'dict';
    protected int $expire = 0;

    public function set($id = '', array $options = [])
    {
        $get = $options['get'] ?? 'data';
        // 获取用户字典列表
        if ($get == 'list') {
            $list = db('dict')->where('type_id=1 AND status=1')->order('sort ASC,id ASC')->column('title', 'id');
            return ['=未绑定='] + $list;
        }
        // 获取字典数据
        $pre_options = $options['pre_options'] ?? []; // 前置选项
        $where_field = is_numeric($id) ? 'id' : 'sign';
        $data = db('dict')->where($where_field, $id)->value('data');
        $list = str_to_array($data);
        return $pre_options + $list;
    }
}