<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2024 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;

/**
 * 根据权限id获取权限列表
 */
class AuthNode extends Widget
{
    protected string $tag = 'auth_node';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        if (empty($id)) {
            return [];
        }
        $auth = [];
        $node = db('auth_node')->field('id,controller,method')->where('id', 'in', $id)->where('status=1')->select();
        foreach ($node as $vo) {
            $auth[$vo['id']] = $vo['controller'].'/'.$vo['method'];
        }
        return $auth;
    }
}