<?php
/**
 * 后台函数库
 */
declare(strict_types=1);
// 权限验证
function check_auth(string $auth_name = ''): int
{
    $user = session('user');
    if (empty($user)) {
        return 0; // 未登录时
    }
    if ($user['group_id'] == 1) {
        return 1; // 超级管理
    }
    $rbac = config_get('rbac', []);
    $no_auth_controller = $rbac['no_auth_controller'] ?? ['index', 'error', 'profile', 'api']; // 不需要验证的控制器
    $controller = get_controller();
    if (empty($auth_name)) {
        $action = get_action();
    } else {
        [$controller, $action] = split_prefix_name($auth_name, $controller, '/');
    }
    if (in_array($controller, $no_auth_controller)) {
        return 1; // 无需验证控制器
    }
    if (!empty($rbac['no_auth_prefix']) && preg_match('/^' . $rbac['no_auth_prefix'] . '\w{1,12}$/', $action)) {
        return 1; // 无需验证方法的前缀
    }
    $no_auth_action = $rbac['no_auth_action'] ?? []; // 无需验证的方法
    if (in_array($action, $no_auth_action) || in_array($controller . '/' . $action, $no_auth_action)) {
        return 1; // 无需验证方法
    }
    $auth = widget('auth')->get($user['group_id']); // 用户权限
    if (empty($auth['auth_node'])) {
        return 0; // 未设置权限
    }
    $node_ids = ids_filter($auth['auth_node']); // 用户权限节点ID
    $node_list = widget('auth_node')->get($node_ids); // 获取用户权限列表
    return in_array($controller . '/' . $action, $node_list) ? 1 : 0; // 验证权限
}

// 获取隐藏样式
function auth_css_hide(string $auth = ''): string
{
    return check_auth($auth) ? '' : ' layui-hide';
}

// 获取验证值：true/false
function auth_value_bool(string $auth = ''): string
{
    return check_auth($auth) ? 'true' : 'false';
}

// 菜单树处理
function menu_tree(array $menu, int $pid = 1): array
{
    $tree = [];
    foreach ($menu as $v) {
        $v['href'] = !empty($v['href']) ? url($v['href']) : '';
        $v['openType'] = $v['target'];
        if (isset($v['icon'])) {
            $v['icon'] = 'layui-icon ' . $v['icon'];
        }
        if ($v['pid'] == $pid) {
            $v['children'] = menu_tree($menu, $v['id']);
            $tree[] = $v;
        }
    }
    return $tree;
}

// 弹窗样式
function dialog_css(): string
{
    $is_dialog = input('get.dialog', 0, 'intval');
    return $is_dialog ? 'is-dialog' : '';
}

// tab高亮显示
function tab_active(string $nav, string $current, string $class = ' class="layui-this"'): string
{
    return ($nav == $current) ? $class : '';
}

// form-select生成
function form_select(string $name, $options, $selected = '', string $attr = ''): string
{
    if (!is_array($options)) {
        $options = str_to_array($options);
    }
    if (!empty($attr)) {
        $attr = ' ' . clear_html($attr);
    }
    $selected = is_array($selected) ? $selected : explode(',', strval($selected));
    $select = '<select name="' . $name . '"' . $attr . '>' . "\n";
    foreach ($options as $k => $v) {
        $select .= "\t<option value=\"{$k}\"" . (in_array($k, $selected) ? ' selected="selected"' : '') . ">{$v}</option>\n";
    }
    return $select . "</select>\n";
}

// form-radio生成
function form_radio(string $name, $options, $selected = '', string $attr = '', bool $is_label = false): string
{
    if (!is_array($options)) {
        $options = str_to_array($options);
    }
    if (!empty($attr)) {
        $attr = ' ' . clear_html($attr);
    }
    $radio = '';
    foreach ($options as $k => $v) {
        if ($is_label) {
            $radio .= '<label' . $attr . '><input type="radio" name="' . $name . '" value="' . $k . '"' . ($k == $selected ? ' checked="checked"' : '') . ' />' . $v . '</label>' . "\n";
        } else {
            $radio .= '<input type="radio" name="' . $name . '" value="' . $k . '" title="' . $v . '"' . ($k == $selected ? ' checked="checked"' : '') . ' />';
        }
    }
    return $radio;
}

// 生成selectList
function select_list(array $options): string
{
    $str = '';
    foreach ($options as $k => $v) {
        if (is_numeric($k) && $k < 0) {
            $k = "'$k'";
        }
        $str .= $k . ': \'' . $v . '\',';
    }
    return '{' . trim($str, ',') . '}';
}