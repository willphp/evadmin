<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
// 生成时间：2025-03-02 13:16:35
class Product extends Model
{
	protected string $table = 'product';
	protected string $pk = 'id';

	protected array $validate = [
		['title', 'required', '标题必填', IF_MUST, AC_BOTH],
		['price', 'money', '价格格式错误', IF_MUST, AC_BOTH],
		['demo_link', 'url', '链接地址错误', IF_VALUE, AC_BOTH],
		['git_link', 'url', '链接地址错误', IF_VALUE, AC_BOTH],
		['sort', 'number', '排序必须是正数', IF_ISSET, AC_BOTH],
	];

	protected array $auto = [
		['status', '1', 'string', IF_MUST, AC_INSERT],
	];

}