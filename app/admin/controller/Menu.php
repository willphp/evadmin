<?php
declare(strict_types=1);
namespace app\admin\controller;
// 菜单权限
class Menu extends Base
{
    protected string $model = 'menu';
    protected string $order = 'sort ASC,id ASC';
    protected int $limit = 0;
    protected  int $formSearch = 0; // 开启搜索表单

    protected function _list_parse(array $list): array
    {
        return menu_tree($list);
    }
}