<?php
declare(strict_types=1);
namespace app\admin\controller;
// 生成时间： 2025-03-02 13:19:12
class Slide extends Base
{
    protected string $model = 'slide'; // 模型表2
    protected string $order = 'sort ASC,id DESC'; // 列表排序
    protected int $limit = 10; // 列表获取条数(0获取全部)
    protected string $fieldExcept = 'content'; // 列表排除字段,多个用,分开
    protected int $formSearch = 1; // 开启搜索表单
    protected int $urlSearch = 0; // 地址栏参数搜索
    protected int $isRecycle = 0; // 回收站开关

}