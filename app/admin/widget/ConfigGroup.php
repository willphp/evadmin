<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
class ConfigGroup extends Widget
{
    protected string $tag = 'config_group';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        $bind = ['=选择分组='];
        $list = db('config_group')->where('status=1')->order('sort ASC,id ASC')->column('title', 'id');
        $bind += $list;
        return $bind[$id] ?? $bind;
    }
}