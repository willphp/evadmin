<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2024 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\common\widget;
use aphp\core\Widget;

/**
 * 栏目数据部件
 */
class Cate extends Widget
{
    protected string $tag = 'cate';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        $map = [];
        if (is_numeric($id)) {
            $map['id'] = $id;
        } else {
            $map['sign'] = $id;
        }
        $fields = 'id,title,sign,tpl,type,link,target,banner,subtitle,summary,content,html_code,update_time,pid,is_parent,level,model_id,model_table,seo_title,seo_kw,seo_desc';
        $vo = db('cate')->field($fields)->where($map)->where('status=1')->find();
        if (!empty($vo)) {
            // 父级导航ID
            $vo['nav_pid'] = ($vo['is_parent'] == 1 || $vo['level'] == 1) ? $vo['id'] : $vo['pid'];
            // 父级栏目标识
            $vo['p_sign'] = $vo['sign'];
            if ($vo['level'] > 1) {
                $vo['p_sign'] = db('cate')->where('id', $vo['pid'])->value('sign');
            }
            // SEO
            $site = site();
            if (empty($vo['seo_title'])) {
                $vo['seo_title'] = $vo['title'] . ' - ' . $site['site_name'];
            }
            if (empty($vo['seo_kw'])) {
                $vo['seo_kw'] = $site['site_kw'];
            }
            if (empty($vo['seo_desc'])) {
                $vo['seo_desc'] = $site['site_desc'];
            }
            // 二级栏目
            if ($vo['is_parent'] == 0 && $vo['level'] == 1) {
                $vo['sub_nav'] = [];
                $vo['nav_title'] = $vo['title'];
                $vo['nav_href'] = ($vo['type'] == 3) ? url($vo['link']) : url('@' . $vo['sign']);
            } else {
                $vo['sub_nav'] = widget('common.sub_nav')->get($vo['nav_pid'], ['active_id' => $vo['id']]);
                $vo['nav_title'] = $vo['sub_nav'][$vo['id']]['title'];
                $vo['nav_href'] = $vo['sub_nav'][$vo['id']]['href'];
            }
        }
        return $vo;
    }
}