-- 清空表: aphp_admin --
DROP TABLE IF EXISTS `aphp_admin`;
-- <fen> --
-- 清空表: aphp_admin_group --
DROP TABLE IF EXISTS `aphp_admin_group`;
-- <fen> --
-- 清空表: aphp_admin_log --
DROP TABLE IF EXISTS `aphp_admin_log`;
-- <fen> --
-- 清空表: aphp_attach --
DROP TABLE IF EXISTS `aphp_attach`;
-- <fen> --
-- 清空表: aphp_auth --
DROP TABLE IF EXISTS `aphp_auth`;
-- <fen> --
-- 清空表: aphp_auth_node --
DROP TABLE IF EXISTS `aphp_auth_node`;
-- <fen> --
-- 清空表: aphp_cate --
DROP TABLE IF EXISTS `aphp_cate`;
-- <fen> --
-- 清空表: aphp_config --
DROP TABLE IF EXISTS `aphp_config`;
-- <fen> --
-- 清空表: aphp_config_group --
DROP TABLE IF EXISTS `aphp_config_group`;
-- <fen> --
-- 清空表: aphp_dict --
DROP TABLE IF EXISTS `aphp_dict`;
-- <fen> --
-- 清空表: aphp_field --
DROP TABLE IF EXISTS `aphp_field`;
-- <fen> --
-- 清空表: aphp_links --
DROP TABLE IF EXISTS `aphp_links`;
-- <fen> --
-- 清空表: aphp_menu --
DROP TABLE IF EXISTS `aphp_menu`;
-- <fen> --
-- 清空表: aphp_message --
DROP TABLE IF EXISTS `aphp_message`;
-- <fen> --
-- 清空表: aphp_model --
DROP TABLE IF EXISTS `aphp_model`;
-- <fen> --
-- 清空表: aphp_model_field --
DROP TABLE IF EXISTS `aphp_model_field`;
-- <fen> --
-- 清空表: aphp_news --
DROP TABLE IF EXISTS `aphp_news`;
-- <fen> --
-- 清空表: aphp_product --
DROP TABLE IF EXISTS `aphp_product`;
-- <fen> --
-- 清空表: aphp_single --
DROP TABLE IF EXISTS `aphp_single`;
-- <fen> --
-- 清空表: aphp_slide --
DROP TABLE IF EXISTS `aphp_slide`;
-- <fen> --
-- 清空表: aphp_tags --
DROP TABLE IF EXISTS `aphp_tags`;
-- <fen> --
