<?php
declare(strict_types=1);
namespace {{$namespace}}\controller;
// 生成时间： {{:date('Y-m-d H:i:s')}}
class {{$class}} extends Base
{
    protected string $model = '{{$table_name}}'; // 模型表2
    protected string $order = '{{$list_order|default='id DESC'}}'; // 列表排序
    protected int $limit = {{$list_limit|default='10'}}; // 列表获取条数(0获取全部)
    protected string $fieldExcept = '{{$list_except|default='content'}}'; // 列表排除字段,多个用,分开
    protected int $formSearch = {{$form_search|default='1'}}; // 开启搜索表单
    protected int $urlSearch = {{$url_search|default='0'}}; // 地址栏参数搜索
    protected int $isRecycle = {{$is_recycle|default='0'}}; // 回收站开关

}