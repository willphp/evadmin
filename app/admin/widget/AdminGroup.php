<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 获取用户组列表
class AdminGroup extends Widget
{
    protected string $tag = 'admin_group';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        $list = db('admin_group')->where('status', 1)->order('id ASC')->column('title', 'id');
        if ($id == 'all') {
            return $list;
        }
        return $list[$id] ?? ['=请选择='] + $list;
    }
}