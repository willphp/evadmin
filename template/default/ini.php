<?php
return array (
  'sign' => 'default',
  'title' => '默认主题',
  'intro' => '默认主题简介',
  'version' => '1.0',
  'price' => '免费',
  'author' => '无念',
  'link' => 'https://www.aphpcms.com',
  'id' => 0,
  'image' => '/static/themes/default/screenshot.jpg',
  'sort' => 9999,
  'time' => '2025-03-02',
);