<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2024 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\common\widget;
use aphp\core\Widget;

/**
 * 标签列表
 */
class Tags extends Widget
{
    protected string $tag = 'tags';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        $field = $options['field'] ?? 'title';
        $map = [];
        if (!empty($id)) {
            $map[] = ['id', 'in', $id];
        }
        return db('tags')->where($map)->where('status=1')->order('sort ASC,id ASC')->column($field, 'sign');
    }
}