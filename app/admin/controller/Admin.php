<?php
declare(strict_types=1);
namespace app\admin\controller;
// 账户管理
class Admin extends Base
{
    protected string $model = 'admin';
    protected string $order = 'id DESC';
    protected string $fieldExcept = 'password,bio,work_menu'; // 排除字段
    protected array $denyFill = ['username']; // 禁止修改的字段

    // 删除头像
    public function remove_avatar(int $id)
    {
        $admin = db('admin');
        $vo = $admin->field('id,avatar')->where('id', $id)->find();
        if (empty($vo['avatar'])) {
            $this->error('头像不存在');
        }
        $r = $admin->where('id', $id)->setField('avatar', '');
        if ($r) {
            $file = ROOT_PATH . '/public' . trim($vo['avatar'], '.');
            if (file_exists($file)) {
                unlink($file);
            }
            $this->success('删除头像成功', 'index');
        }
        $this->error('删除失败');
    }
}