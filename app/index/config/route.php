<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2025 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
/**
 * 路由配置
 */
return [
    'empty_jump_to' => true, // 开启空跳转
    'jump_to' => [
        'class' => 'app\\index\\controller\\Index', // 空控制器
        'action' => 'index', // 空方法
        'params' => 'act', // 原路径参数
    ],
];