<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
class Tags extends Widget
{
    protected string $tag = 'tags';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        if (!empty($id)) {
            return db('tags')->where('id', $id)->where('status=1')->value('title');
        }
        return db('tags')->where('status=1')->order('id ASC')->column('title', 'id');
    }
}