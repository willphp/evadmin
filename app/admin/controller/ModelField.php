<?php
declare(strict_types=1);
namespace app\admin\controller;
use aphp\core\Tool;
// 模型字段
class ModelField extends Base
{
    protected string $model = 'model_field';
    protected string $order = 'sort ASC,id ASC';
    protected int $modelId = 0;

    public function __construct()
    {
        $this->modelId = input('model_id', 0, 'intval');
        if ($this->modelId == 0) {
            $this->error('非法操作');
        }
        $this->jumpUrl .= '?model_id=' . $this->modelId;
        view_with('model_id', $this->modelId);
    }

    protected function _index_where(): array
    {
        return ['model_id' => $this->modelId];
    }

    protected function _list_parse(array $list): array
    {
        $table = widget('model')->get($this->modelId);
        $is_table = db()->hasTable($table);
        $field_list = $is_table ? db()->getFields($table) : [];
        foreach ($list as &$vo) {
            $vo['is_table_field'] = in_array($vo['field_name'], $field_list) ? 1 : 0;
        }
        return $list;
    }

    // 存入预设
    public function preset(int $model_id, string $id)
    {
        $vo = db('model_field')->field('id,model_id', true)->where('id', $id)->find();
        if (!$vo) {
            $this->error('字段不存在');
        }
        $field = db('field');
        $vo['label'] = $vo['field_title'];
        $r = $field->insert($vo);
        $this->_jump(['存入预设成功', '存入预设失败'], $r, 'index');
    }

    // 导出
    public function export(int $model_id, string $ids = '')
    {
        $name = widget('model')->get($model_id);
        if (!$name) {
            $this->error('模型不存在');
        }
        $map = [];
        $map['model_id'] = $model_id;
        $map['status'] = 1;
        if (!empty($ids)) {
            $ids = ids_filter($ids, true);
            if (count($ids) == 1) {
                $map['id'] = $ids[0];
            } else {
                $map[] = ['id', 'in', $ids];
            }
        }
        $field_list = db('model_field')->field('id,model_id', true)->where($map)->order('sort ASC,id ASC')->select();
        $file = 'uploads/model/field_'.$name.'.csv';
        $path = ROOT_PATH.'/public/'.$file;
        Tool::dir_init(dirname($path));
        $handle = fopen($path, 'w');
        fputcsv($handle, array_keys($field_list[0]));
        foreach ($field_list as $vo) {
            $data = array_map(fn($v)=>mb_convert_encoding($v, 'GBK', 'UTF-8'), $vo);
            fputcsv($handle, $data);
        }
        fclose($handle);
        $this->_url(__HOST__.'/'.$file);
    }

    // 导入
    public function import(int $model_id, array $req)
    {
        $name = widget('model')->get($model_id);
        if (!$name) {
            $this->error('模型不存在');
        }
        if ($this->isPost()) {
            if (empty($req['field_ids']) && empty($req['data_file'])) {
                $this->error('请选择导入字段或上传CSV文件');
            }
            $model_field = db('model_field');
            if ($req['is_clear'] == 1) {
                $model_field->where('model_id', $model_id)->delete(); // 删除原字段
                cli('remove:table '.$name); // 删除原表
            }
            if (!empty($req['field_ids'])) {
                $fields = db('field')->field('id,title', true)->where('id', 'in', $req['field_ids'])->order('sort ASC,id ASC')->select();
                foreach ($fields as $vo) {
                    $vo['model_id'] = $model_id;
                    $field_id = $model_field->where('field_name', $vo['field_name'])->where('model_id', $model_id)->value('id');
                    if ($field_id) {
                        $vo['field_name'] .= '_'.$field_id; // 有相同字段名时处理
                    }
                    $model_field->insert($vo);
                }
            } else {
                $file = trim($req['data_file'],'/');
                $path = ROOT_PATH.'/public/'.$file;
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                if ($ext != 'csv') {
                    $this->error('必须是csv文件');
                }
                if (!file_exists($path)) {
                    $this->error('文件不存在');
                }
                $handle = fopen($path, 'r');
                $i = 0;
                $field_key = [];
                while (($data = fgetcsv($handle, 1000)) !== false) {
                    if ($i == 0) {
                        $field_key = $data;
                        array_unshift($field_key, 'model_id');
                    }
                    if ($i > 0) {
                        $field_value = array_map(fn($v)=>mb_convert_encoding($v, 'UTF-8', 'GBK'), $data);
                        array_unshift($field_value, $model_id);
                        $save = array_combine($field_key, $field_value);
                        $field_id = $model_field->where('field_name', $save['field_name'])->where('model_id', $model_id)->value('id');
                        if ($field_id) {
                            $save['field_name'] .= '_'.$field_id; // 有相同字段名时处理
                        }
                        $model_field->insert($save);
                    }
                    $i ++;
                }
                fclose($handle);
            }
            $this->success('导入成功，相同字段名会重命名');
        }
        return view();
    }
}