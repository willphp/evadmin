<?php
declare(strict_types=1);
namespace app\admin\controller;
class Dict extends Base
{
    protected string $model = 'dict';
    protected string $order = 'sort ASC,id DESC';
    protected string $fieldExcept = 'data';
}