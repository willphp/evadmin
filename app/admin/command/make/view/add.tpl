<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8"/>
    <title>添加</title>
    <link href="__STATIC__/component/pear/css/pear.css" rel="stylesheet"/>
    {include file='public/head.html'}
</head>
<body class="childrenBody {:dialog_css()}">

<form class="layui-form" action="{:url('add')}" method="post">

    {{$form_items}}

    <div class="layui-form-item layer-footer">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn layui-btn-normal layui-btn-sm" lay-submit="" lay-filter="save">
                <i class="layui-icon layui-icon-ok"></i>
                添加
            </button>
            <button type="reset" class="layui-btn layui-btn-primary layui-btn-sm">
                <i class="layui-icon layui-icon-refresh"></i>
                重置
            </button>
        </div>
    </div>
</form>

<script src="__STATIC__/component/layui/layui.js"></script>
<script src="__STATIC__/component/pear/pear.js"></script>
<script>
    layui.use(['aphpForm', 'aphp', 'form'], function () {
        var aphpForm = layui.aphpForm,
            aphp = layui.aphp,
            form = layui.form;

        aphpForm.bindEvent('form.layui-form');

        form.on('submit(save)', function (data) {
            aphp.request.postForm(data, 0, function () {
                parent.layer.close(parent.layer.getFrameIndex(window.name));
                parent.layui.table.reload('currentTable');
            });
            return false;
        });
    })
</script>
</body>
</html>