<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2024 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\common\widget;
use aphp\core\Widget;

/**
 * 上下关联数据
 */
class Related extends Widget
{
    protected string $tag = 'news';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        $order = $options['order'] ?? 'id DESC';
        $where = $options['where'] ?? [];
        return db($id)->where($where)->where('status=1')->order($order)->column('title', 'id');
    }
}