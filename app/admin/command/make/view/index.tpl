<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8"/>
    <title>列表</title>
    <link href="__STATIC__/component/pear/css/pear.css" rel="stylesheet"/>
    {include file='public/head.html'}
</head>
<body class="pear-container">

<div class="layui-card">
    <div class="layui-card-body">
        <table class="layui-hide" id="currentTable" lay-filter="currentTable"
               data-auth-add="{:check_auth('add')}"
               data-auth-edit="{:check_auth('edit')}"
               data-auth-delete="{:check_auth('del')}"
               data-auth-modify="{:check_auth('multi')}"
        ></table>
    </div>
</div>

<script src="__STATIC__/component/layui/layui.js"></script>
<script src="__STATIC__/component/pear/pear.js"></script>
<script>
    layui.use(['aphpTable'], function () {
        var aphpTable = layui.aphpTable;
        var $ = layui.$;

        var init = {
            table_elem: '#currentTable',
            table_render_id: 'currentTable',
            add_url: "{:url('add')}",
            edit_url: "{:url('edit')}",
            delete_url: "{:url('del')}",
            modify_url: "{:url('multi')}",
        };

        aphpTable.render({
            init: init,
            url: "{:url('index')}",
            where: {{$url_search == '1' ? '{ where: location.search.slice(1) }' : '{}'}},
            toolbar: ['refresh_table', 'add', 'enable', 'disable', 'delete'],
            search: {{$form_search == '1' ? 'true' : 'false'}}, // 表单搜索
            quickSearch: '{{$quick_search}}', // 快速搜索设置
            searchFormVisible: {{$form_visible == '1' ? 'true' : 'false'}}, //默认显示顶部搜索
            showSearch: {{$form_toggle == '1' ? 'true' : 'false'}}, //工具栏是否显示搜索按钮
            cols: [[
                { type: 'checkbox'},
                {{$table_cols}}
                { title: '操作', align: 'center', width: 120, templet: aphpTable.formatter.tool, operat: ['edit','delete']}
            ]],
            {{$is_page == '1' ? 'page: {}' : 'page: false'}}
        });
        aphpTable.bindEvent();
    });
</script>
</body>
</html>