<?php
declare(strict_types=1);
namespace {{$namespace}}\model;
use aphp\core\Model;
// 生成时间：{{:date('Y-m-d H:i:s')}}
class {{$class}} extends Model
{
	protected string $table = '{{$table_name}}';
	protected string $pk = '{{$pk|default='id'}}';
{{$verify}}
{{$auto}}
{{$filter}}
}