<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
// 生成时间：2025-03-02 13:11:07
class Tags extends Model
{
	protected string $table = 'tags';
	protected string $pk = 'id';

	protected array $validate = [
		['title', 'required', '标题必填', IF_MUST, AC_BOTH],
		['sign', 'string|unique', '标识格式错误|标识已存在', IF_MUST, AC_BOTH],
		['sort', 'number', '排序必须是正数', IF_ISSET, AC_BOTH],
	];

	protected array $auto = [
		['status', '1', 'string', IF_MUST, AC_INSERT],
	];

}