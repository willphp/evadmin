-- 表结构: aphp_admin --
CREATE TABLE `aphp_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '分组ID',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '账户',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '密码',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `bio` varchar(120) NOT NULL DEFAULT '' COMMENT '格言',
  `avatar` varchar(100) NOT NULL DEFAULT '' COMMENT '头像',
  `qq` varchar(16) NOT NULL DEFAULT '' COMMENT 'QQ',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `mobile` char(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `work_menu` varchar(255) NOT NULL DEFAULT '' COMMENT '快捷菜单',
  `login_ip` int(11) NOT NULL DEFAULT '0' COMMENT '登录IP',
  `login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';
-- <fen> --
-- 表结构: aphp_admin_group --
CREATE TABLE `aphp_admin_group` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '标题',
  `memo` varchar(120) NOT NULL DEFAULT '' COMMENT '备注',
  `auth_menu` text NOT NULL COMMENT '菜单权限',
  `auth_node` text NOT NULL COMMENT '节点权限',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='管理分组';
-- <fen> --
-- 表结构: aphp_admin_log --
CREATE TABLE `aphp_admin_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `admin_name` varchar(20) DEFAULT '' COMMENT '管理员名',
  `title` varchar(100) DEFAULT '' COMMENT '操作',
  `content` text NOT NULL COMMENT '内容',
  `controller` varchar(20) NOT NULL DEFAULT '' COMMENT '控制器',
  `method` varchar(20) NOT NULL DEFAULT '' COMMENT '方法',
  `user_ip` varchar(30) DEFAULT '' COMMENT '操作IP',
  `user_agent` varchar(255) DEFAULT '' COMMENT 'User-Agent',
  `create_time` int(10) unsigned DEFAULT '0' COMMENT '操作时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理日志';
-- <fen> --
-- 表结构: aphp_attach --
CREATE TABLE `aphp_attach` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '文件类型',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '文件名称',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `ext` char(6) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `upload_api` varchar(20) NOT NULL DEFAULT '' COMMENT '上传接口',
  `admin_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `upload_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='附件表';
-- <fen> --
-- 表结构: aphp_auth --
CREATE TABLE `aphp_auth` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '权限名',
  `controller` varchar(20) NOT NULL DEFAULT '' COMMENT '控制器',
  `model_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '模型ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='权限表';
-- <fen> --
-- 表结构: aphp_auth_node --
CREATE TABLE `aphp_auth_node` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `auth_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '权限ID',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '节点名',
  `controller` varchar(20) NOT NULL DEFAULT '' COMMENT '控制器',
  `method` varchar(20) NOT NULL DEFAULT '' COMMENT '方法',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `auth_id` (`auth_id`),
  KEY `controller` (`controller`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COMMENT='权限节点';
-- <fen> --
-- 表结构: aphp_cate --
CREATE TABLE `aphp_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '标题',
  `sign` varchar(30) NOT NULL DEFAULT '' COMMENT '标识',
  `tpl` varchar(30) NOT NULL DEFAULT '' COMMENT '模板',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1单页2列表3外链',
  `link` varchar(100) NOT NULL DEFAULT '' COMMENT '外链',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '打开方式',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '显示',
  `banner` varchar(200) NOT NULL DEFAULT '' COMMENT '栏目图',
  `subtitle` varchar(100) NOT NULL DEFAULT '' COMMENT '副标题',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `content` text NOT NULL COMMENT '内容',
  `html_code` text NOT NULL COMMENT 'HTML代码',
  `seo_title` varchar(100) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_kw` varchar(255) NOT NULL DEFAULT '' COMMENT 'SEO关键字',
  `seo_desc` varchar(255) NOT NULL DEFAULT '' COMMENT 'SEO描述',
  `model_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '模型ID',
  `model_table` varchar(30) NOT NULL DEFAULT '' COMMENT '模型表',
  `is_parent` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '父栏目',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '层次',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `sub_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '子统计',
  `tree_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '树统计',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `sign` (`sign`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='栏目表';
-- <fen> --
-- 表结构: aphp_config --
CREATE TABLE `aphp_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '分组ID',
  `field_title` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `field_name` varchar(20) NOT NULL DEFAULT '' COMMENT '键名',
  `field_value` varchar(255) NOT NULL DEFAULT '' COMMENT '键值',
  `field_comment` varchar(50) NOT NULL DEFAULT '' COMMENT '说明',
  `form_required` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '必填标记',
  `input_style` varchar(100) NOT NULL DEFAULT 'layui-input-block' COMMENT '容器样式',
  `form_type` varchar(20) NOT NULL DEFAULT 'text' COMMENT '表单类型',
  `form_tips` varchar(50) NOT NULL DEFAULT '' COMMENT '输入提示',
  `lay_verify` varchar(30) NOT NULL DEFAULT '' COMMENT '前端验证',
  `form_attr` varchar(255) NOT NULL DEFAULT '' COMMENT '表单属性',
  `form_options` varchar(255) NOT NULL DEFAULT '' COMMENT '选项设置',
  `is_sys` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0可删1禁删',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COMMENT='配置表';
-- <fen> --
-- 表结构: aphp_config_group --
CREATE TABLE `aphp_config_group` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '标题',
  `memo` varchar(200) NOT NULL DEFAULT '' COMMENT '备注',
  `app` varchar(20) NOT NULL DEFAULT 'index' COMMENT '所属应用',
  `sign` varchar(30) NOT NULL DEFAULT '' COMMENT '标识',
  `is_hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0显示1隐藏',
  `is_sys` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0可删1禁删',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='配置分组表';
-- <fen> --
-- 表结构: aphp_dict --
CREATE TABLE `aphp_dict` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `memo` varchar(150) NOT NULL DEFAULT '' COMMENT '备注',
  `sign` varchar(30) NOT NULL DEFAULT '' COMMENT '英文标识',
  `data` text NOT NULL COMMENT '数据',
  `type_id` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1用户2系统',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='数据字典';
-- <fen> --
-- 表结构: aphp_field --
CREATE TABLE `aphp_field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `label` varchar(20) NOT NULL DEFAULT '' COMMENT '标签名',
  `type_id` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0可选1必须2ID',
  `field_title` varchar(20) NOT NULL DEFAULT '' COMMENT '字段标题',
  `field_name` varchar(20) NOT NULL DEFAULT '' COMMENT '字段名称',
  `field_type` varchar(20) NOT NULL DEFAULT 'varchar' COMMENT '字段类型',
  `field_length` varchar(10) NOT NULL DEFAULT '0' COMMENT '字段长度',
  `field_value` varchar(50) NOT NULL DEFAULT 'null' COMMENT '字段默认值',
  `field_comment` varchar(20) NOT NULL DEFAULT '' COMMENT '字段注释',
  `is_required` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '必填',
  `is_unsigned` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '正数',
  `is_auto_increment` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自增',
  `is_primary` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '主键',
  `is_index` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '索引',
  `show_list` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '列表显示',
  `show_add` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新增显示',
  `show_edit` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '编辑显示',
  `col_search` varchar(20) NOT NULL DEFAULT 'true' COMMENT '查询设置',
  `col_search_op` varchar(20) NOT NULL DEFAULT '=' COMMENT '查询方式',
  `col_attr` varchar(250) NOT NULL DEFAULT '' COMMENT '列表属性',
  `col_tpl` varchar(250) NOT NULL DEFAULT '' COMMENT '列表模板',
  `form_required` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '必填标记',
  `input_style` varchar(100) NOT NULL DEFAULT 'layui-input-block' COMMENT '容器样式',
  `form_type` varchar(20) NOT NULL DEFAULT 'text' COMMENT '表单类型',
  `form_tips` varchar(100) NOT NULL DEFAULT '' COMMENT '输入提示',
  `form_value` varchar(50) NOT NULL DEFAULT '' COMMENT '表单默认值',
  `lay_verify` varchar(50) NOT NULL DEFAULT 'none' COMMENT '前端验证',
  `form_attr` varchar(250) NOT NULL DEFAULT '' COMMENT '表单属性',
  `form_options` varchar(250) NOT NULL DEFAULT '' COMMENT '选项设置',
  `dict_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '绑定字典ID',
  `is_verify` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自动验证',
  `verify_rule` varchar(100) NOT NULL DEFAULT '' COMMENT '验证规则',
  `verify_msg` varchar(200) NOT NULL DEFAULT '' COMMENT '验证提示',
  `verify_if` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '验证条件',
  `verify_scene` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '验证场景',
  `is_auto` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自动处理',
  `auto_method` char(16) NOT NULL DEFAULT 'string' COMMENT '处理方式',
  `auto_rule` varchar(100) NOT NULL DEFAULT '' COMMENT '处理规则',
  `auto_if` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '处理条件',
  `auto_scene` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '处理场景',
  `is_filter` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自动过滤',
  `filter_if` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '过滤条件',
  `filter_scene` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '过滤场景',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COMMENT='字段预设表';
-- <fen> --
-- 表结构: aphp_links --
CREATE TABLE `aphp_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `logo` varchar(200) NOT NULL DEFAULT '' COMMENT '封面',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT '链接',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='友链表';
-- <fen> --
-- 表结构: aphp_menu --
CREATE TABLE `aphp_menu` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '层次',
  `path` varchar(250) NOT NULL DEFAULT '' COMMENT '路径',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `auth` varchar(50) NOT NULL DEFAULT '' COMMENT '权限',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0目录1菜单',
  `href` varchar(100) NOT NULL DEFAULT '' COMMENT '链接',
  `target` varchar(20) NOT NULL DEFAULT '_iframe' COMMENT '打开方式',
  `model_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '模型ID',
  `is_sys` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0可删1禁删',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COMMENT='后台菜单';
-- <fen> --
-- 表结构: aphp_message --
CREATE TABLE `aphp_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `nickname` varchar(20) NOT NULL DEFAULT '' COMMENT '昵称',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `msg` varchar(255) NOT NULL DEFAULT '' COMMENT '留言',
  `ip` varchar(20) NOT NULL DEFAULT '' COMMENT 'IP地址',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '已读',
  `reply` varchar(255) NOT NULL DEFAULT '' COMMENT '回复',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='留言表';
-- <fen> --
-- 表结构: aphp_model --
CREATE TABLE `aphp_model` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '标题',
  `table_name` varchar(20) NOT NULL DEFAULT '' COMMENT '表名标识',
  `table_prefix` varchar(10) NOT NULL DEFAULT '' COMMENT '表前缀',
  `table_comment` varchar(50) NOT NULL DEFAULT '' COMMENT '表注释',
  `table_engine` varchar(10) NOT NULL DEFAULT 'InnoDB' COMMENT '存储引擎',
  `table_charset` varchar(20) NOT NULL DEFAULT 'utf8mb4' COMMENT '字符集',
  `table_collate` varchar(50) NOT NULL DEFAULT 'utf8mb4_general_ci' COMMENT '排序规则',
  `table_primary` varchar(20) NOT NULL DEFAULT 'id' COMMENT '表主键',
  `list_order` varchar(50) NOT NULL DEFAULT 'id DESC' COMMENT '列表排序',
  `list_limit` smallint(5) unsigned NOT NULL DEFAULT '10' COMMENT '每页条数',
  `list_except` varchar(100) NOT NULL DEFAULT 'content' COMMENT '列表排除字段',
  `form_search` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '表单搜索',
  `form_visible` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '搜索可见',
  `form_toggle` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '切换按钮',
  `quick_search` varchar(100) NOT NULL DEFAULT '' COMMENT '快搜设置',
  `url_search` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'URL条件',
  `is_recycle` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '回收站',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='模型表';
-- <fen> --
-- 表结构: aphp_model_field --
CREATE TABLE `aphp_model_field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `model_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '模型ID',
  `type_id` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0可选1必须2ID',
  `field_title` varchar(20) NOT NULL DEFAULT '' COMMENT '字段标题',
  `field_name` varchar(20) NOT NULL DEFAULT '' COMMENT '字段名称',
  `field_type` varchar(20) NOT NULL DEFAULT 'varchar' COMMENT '字段类型',
  `field_length` varchar(10) NOT NULL DEFAULT '0' COMMENT '字段长度',
  `field_value` varchar(50) NOT NULL DEFAULT 'null' COMMENT '字段默认值',
  `field_comment` varchar(20) NOT NULL DEFAULT '' COMMENT '字段注释',
  `is_required` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '必填',
  `is_unsigned` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '正数',
  `is_auto_increment` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自增',
  `is_primary` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '主键',
  `is_index` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '索引',
  `show_list` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '列表显示',
  `show_add` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新增显示',
  `show_edit` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '编辑显示',
  `col_search` varchar(20) NOT NULL DEFAULT 'true' COMMENT '查询设置',
  `col_search_op` varchar(20) NOT NULL DEFAULT '=' COMMENT '查询方式',
  `col_attr` varchar(250) NOT NULL DEFAULT '' COMMENT '列表属性',
  `col_tpl` varchar(250) NOT NULL DEFAULT '' COMMENT '列表模板',
  `form_required` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '必填标记',
  `input_style` varchar(100) NOT NULL DEFAULT 'layui-input-block' COMMENT '容器样式',
  `form_type` varchar(20) NOT NULL DEFAULT 'text' COMMENT '表单类型',
  `form_tips` varchar(100) NOT NULL DEFAULT '' COMMENT '输入提示',
  `form_value` varchar(50) NOT NULL DEFAULT '' COMMENT '表单默认值',
  `lay_verify` varchar(50) NOT NULL DEFAULT 'none' COMMENT '前端验证',
  `form_attr` varchar(250) NOT NULL DEFAULT '' COMMENT '表单属性',
  `form_options` varchar(250) NOT NULL DEFAULT '' COMMENT '选项设置',
  `dict_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '绑定字典ID',
  `is_verify` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自动验证',
  `verify_rule` varchar(100) NOT NULL DEFAULT '' COMMENT '验证规则',
  `verify_msg` varchar(200) NOT NULL DEFAULT '' COMMENT '验证提示',
  `verify_if` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '验证条件',
  `verify_scene` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '验证场景',
  `is_auto` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自动处理',
  `auto_method` char(16) NOT NULL DEFAULT 'string' COMMENT '处理方式',
  `auto_rule` varchar(100) NOT NULL DEFAULT '' COMMENT '处理规则',
  `auto_if` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '处理条件',
  `auto_scene` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '处理场景',
  `is_filter` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '自动过滤',
  `filter_if` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '过滤条件',
  `filter_scene` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '过滤场景',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COMMENT='模型字段表';
-- <fen> --
-- 表结构: aphp_news --
CREATE TABLE `aphp_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `tags_ids` varchar(255) NOT NULL DEFAULT '' COMMENT '标签',
  `thumb` varchar(200) NOT NULL DEFAULT '' COMMENT '封面',
  `content` text NOT NULL COMMENT '内容',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT '购买链接',
  `is_top` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '置顶',
  `post_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `read_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='新闻表';
-- <fen> --
-- 表结构: aphp_product --
CREATE TABLE `aphp_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `color_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '颜色',
  `type_id` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '类别',
  `thumb` varchar(200) NOT NULL DEFAULT '' COMMENT '封面',
  `content` text NOT NULL COMMENT '内容',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `demo_link` varchar(200) NOT NULL DEFAULT '' COMMENT '演示地址',
  `git_link` varchar(200) NOT NULL DEFAULT '' COMMENT '仓库地址',
  `down_zip` varchar(200) NOT NULL DEFAULT '' COMMENT '下载地址',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='产品表';
-- <fen> --
-- 表结构: aphp_single --
CREATE TABLE `aphp_single` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目',
  `sign` varchar(20) NOT NULL DEFAULT '' COMMENT '标题',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='单页表';
-- <fen> --
-- 表结构: aphp_slide --
CREATE TABLE `aphp_slide` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type_id` smallint(5) NOT NULL DEFAULT '0' COMMENT '分类',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `image` varchar(200) NOT NULL DEFAULT '' COMMENT '图片',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT '链接',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '打开方式',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='幻灯表';
-- <fen> --
-- 表结构: aphp_tags --
CREATE TABLE `aphp_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `sign` varchar(20) NOT NULL DEFAULT '' COMMENT '标识',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='标签表';
-- <fen> --
