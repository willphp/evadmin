<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model as CoreModel;
class Model extends CoreModel
{
	protected string $table = 'model';
	protected string $pk = 'id';
    protected array $validate = [
        ['title', 'required|unique', '模型名称必须|模型名称已存在', IF_MUST, AC_BOTH],
        ['table_name', '/^\w{3,20}$/|unique', '表名格式错误|表名已存在', IF_MUST, AC_BOTH],
        ['table_comment', 'required', '表注释必须', IF_MUST, AC_BOTH],
        ['table_primary', 'required', '表主键必须', IF_MUST, AC_INSERT],
        ['list_limit', 'number', '每页条数必须是数字', IF_MUST, AC_UPDATE],
    ];
	protected array $auto = [
        ['status', 1, 'string', IF_MUST, AC_INSERT],
    ];

    // 检测表是否存在
    protected function _before_insert(array &$data): void
    {
        $data['table_prefix'] = $this->prefix;
        if ($this->db->hasTable($data['table_name'])) {
            $this->errors[] = $data['table_name'] . '表已存在';
        }
    }

    // 添加预设模型字段
    protected function _after_insert(array $data): void
    {
        if (!empty($data['field_ids'])) {
            $fields = db('field')->field('id,label', true)->where('id', 'in', $data['field_ids'])->order('sort ASC,id ASC')->select();
            $model_field = db('model_field');
            foreach ($fields as $vo) {
                $vo['model_id'] = intval($data['id']);
                $model_field->insert($vo);
            }
        }
    }

    protected function _after_update(array $before, array $after): void
    {
        // 修改名称后更新菜单名称和权限名称
        if ($after['title'] != $before['title']) {
            db('menu')->where('model_id', $before['id'])->setField('title', $after['title']);
            db('auth')->where('model_id', $before['id'])->setField('title', $after['title']);
        }
        // 更新表名
        if ($this->db->hasTable($before['table_name'])) {
            if ($after['table_name'] != $before['table_name']) {
                $this->db->execute('ALTER TABLE `' . $this->prefix . $before['table_name'] . '` RENAME TO `' . $this->prefix . $after['table_name'] . '`;');
            }
            if ($after['table_comment'] != $before['table_comment']) {
                $this->db->execute('ALTER TABLE `' . $this->prefix . $after['table_name'] . '` COMMENT \'' . $after['table_comment'] . '\';');
            }
        }
    }

    protected function _after_delete(array $data): void
    {
        // 删除表
        $this->db->execute('DROP TABLE IF EXISTS `' . $data['table_prefix'] . $data['table_name'] . '`;');
        // 删除模型字段
        db('model_field')->where('model_id', $data['id'])->delete();
        // 删除后台菜单
        db('menu')->where('model_id', $data['id'])->delete();
        // 删除权限节点
        db('auth')->where('controller', $data['table_name'])->delete();
        db('auth_node')->where('controller', $data['table_name'])->delete();
        // 删除文件
        $name = $data['table_name'];
        $class = name_to_camel($name);
        $view_path = ROOT_PATH . '/app/admin/view/' . strtolower($name); //模板路径
        $file_list = [
            ROOT_PATH . '/app/admin/controller/' . $class . '.php',
            ROOT_PATH . '/app/admin/model/' . $class . '.php',
            $view_path . '/index.html',
            $view_path . '/add.html',
            $view_path . '/edit.html',
            $view_path . '/recycle.html',
        ];
        foreach ($file_list as $file) {
            if (is_file($file)) unlink($file);
        }
        if (is_dir($view_path)) rmdir($view_path);
    }
}