<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2025 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\admin\controller;
use aphp\core\Jump;

class Cms
{
    use Jump;
    protected string $middleware = 'rbac';

    // CMS配置
    public function config()
    {
        return view();
    }

    // 内容管理
    public function index()
    {
        config('debug_bar.is_hide', true);
        $cate = db('cate')->field('id,pid,title,sign,type,link,model_id,tree_count,is_parent')->where('id>1 AND status=1')->order('sort ASC,id ASC')->select();
        foreach ($cate as &$vo) {
            $vo['url'] = 'javascript:;'; // 默认不跳转
            $vo['target'] = 'cms_iframe'; // 默认打开
            if ($vo['type'] == 1) {
                // 单页
                if ($vo['model_id'] > 0) {
                    // 关联模型
                    $name = widget('model')->get($vo['model_id']);
                    $single = db($name);
                    $single_id = $single->where('tid', $vo['id'])->value('id');
                    if (!$single_id) {
                        $data = [];
                        $data['tid'] = $vo['id'];
                        $data['sign'] = $vo['sign'];
                        $data['title'] = $vo['title'];
                        $data['status'] = 1;
                        $single_id = $single->insertGetId($data);
                    }
                    $vo['url'] = url($name.'/edit?id='.$single_id.'&tid='.$vo['id']); // 编辑单页
                } else {
                    // 无模型
                    $vo['url'] = url('edit?id='.$vo['id']); // 编辑栏目
                }
            } elseif ($vo['type'] == 2 && $vo['model_id'] > 0 && $vo['is_parent'] == 0) {
                // 列表
                $name = widget('model')->get($vo['model_id']);
                $vo['url'] = url($name.'/index').'?tid='.$vo['id'];
            } elseif ($vo['type'] == 3) {
                // 外链
                $vo['url'] = url($vo['link']);
                $vo['target'] = '_blank';
            }
            $vo['title'] .= '('.$vo['tree_count'].')';
            if ($vo['is_parent'] > 0) {
                $vo['isParent'] = true;
            }
        }
        $json = json_encode($cate);
        return view()->with('json', $json);
    }

    // 编辑栏目
    public function edit(int $id, array $req)    {
        $model = model('cate')->find($id);
        if (!$model) {
            $this->error('记录不存在');
        }
        if ($this->isPost()) {
            $r = db()->trans(function() use ($model, $req) {
                $model->save($req);
            });
            $this->_jump(['修改成功', '修改失败'], $r, 'index');
        }
        return view()->with('vo', $model->toArray());
    }

    //  CMS统计
    public function home()
    {
        $total = [];
        $total[1] = db('cate')->where('id>1')->count();
        $total[2] = db('news')->count();
        $total[3] = db('product')->count();
        $total[4] = db('message')->count();
        return view()->with('total', $total);
    }
}